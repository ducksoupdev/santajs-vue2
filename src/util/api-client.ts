import {
  AxiosResponse,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosInterceptorManager,
  AxiosPromise
} from 'axios';
import {
  ILogger
} from './log';

class MockAxios implements AxiosInstance {
  defaults: AxiosRequestConfig;
  interceptors: {
    request: AxiosInterceptorManager<AxiosRequestConfig>;
    response: AxiosInterceptorManager<AxiosResponse>
  };

  private apiData: any;

  constructor() {
    this.apiData = require('./api-data.json');
  }

  private emptyPromise() {
    return new Promise<AxiosResponse>((resolve) => {
      resolve(null);
    });
  }

  private getResponsePromise(url: string, method: string, data?: any): AxiosPromise {
    return new Promise<AxiosResponse>((resolve, reject) => {
      let responseData = null,
        status = 404;
      if (this.apiData[method][url]) {
        responseData = this.apiData[method][url].response.data;
        status = this.apiData[method][url].response.status;
      }

      // validate the params if there are any
      if (data != null) {
        let params = this.apiData[method][url].params;
        if (params != null && params.length) {
          params.forEach((param) => {
            if (!data.hasOwnProperty(param)) {
              responseData = null;
              status = 400;
            }
          });
        }
      }

      if (status >= 400) {
        reject({
          response: {
            data: responseData,
            status: status,
            statusText: null,
            headers: null,
            config: null
          }
        });
      } else {
        resolve({
          data: responseData,
          status: status,
          statusText: null,
          headers: null,
          config: null
        });
      }
    });
  }

  request(config: AxiosRequestConfig): AxiosPromise {
    return this.emptyPromise();
  }

  get(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this.getResponsePromise(url, 'get');
  }

  delete(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this.emptyPromise();
  }

  head(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this.emptyPromise();
  }

  post(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise {
    return this.getResponsePromise(url, 'post', data);
  }

  put(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise {
    return this.getResponsePromise(url, 'put', data);
  }

  patch(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise {
    return this.emptyPromise();
  }

}

export class ApiClientFactory {

  static getClient(): AxiosInstance {
    if (process.env.NODE_ENV === 'production') {
      return require('axios').create(null);
    } else {
      return new MockAxios();
    }
  }

}
