import { IAction, IActionType, Store } from './store';
import { FlashActionTypes } from './flash-actions';

const INITIAL_STATE = [];

export class FlashStore extends Store {

  static getInstance(): FlashStore {
    if (!this.storeInstance) this.storeInstance = new this();
    return <FlashStore>this.storeInstance;
  }

  getInitialState() {
    return INITIAL_STATE;
  }

  onDispatch(action: IAction) {
    switch (action.type) {
      case FlashActionTypes.ADD_MESSAGE:
        let newState = [ ...this.state, action.value ];
        this.state = newState;
        this.emitChange();
        break;
      case FlashActionTypes.CLEAR_MESSAGES:
        this.state = [];
        this.emitChange();
        break;
    }
  }

  getMessages() {
    return this.state;
  }

}
