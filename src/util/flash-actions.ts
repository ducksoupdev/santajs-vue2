import { IAction } from './store';

export const FlashActionTypes = {
  ADD_MESSAGE: 'ADD_MESSAGE',
  CLEAR_MESSAGES: 'CLEAR_MESSAGES'
};

export interface IFlashMessage {
  level: string;
  text: string;
}

export class FlashActions {

  static addMessage(value: IFlashMessage): IAction {
    return {
      type: FlashActionTypes.ADD_MESSAGE,
      value: value
    };
  }

  static clearMessages(): IAction {
    return {
      type: FlashActionTypes.CLEAR_MESSAGES
    };
  }

}
