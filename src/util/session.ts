import {AxiosError, AxiosResponse, AxiosInstance} from 'axios';
import {ApiClientFactory, ErrorHandler, FlashStore, FlashActionTypes, Logger, IAccount, ILogin} from '.';

export interface ILoginForm {
  email: string;
  password: string;
}

export interface ILogin {
  account: string;
  autoLogin: string;
  email: string;
}

export interface IAccount {
  name: string;
  shortUrl: string;
  status: string;
}

export class SessionService {
  private loginUrl = '/api/auth';
  private accountUrl = '/api/account';

  private static _instance: SessionService;
  private _currentAccount: IAccount;
  private _currentLogin: ILogin;
  private apiClient: AxiosInstance;

  set currentAccount(value: IAccount) {
    this._currentAccount = value;
  }

  set currentLogin(value: ILogin) {
    this._currentLogin = value;
  }

  get currentAccount(): IAccount {
    return this._currentAccount;
  }

  get currentLogin(): ILogin {
    return this._currentLogin;
  }

  private constructor() {
    this.apiClient = ApiClientFactory.getClient();
  }

  static createInstance() {
    SessionService.getInstance();
  }

  static getInstance() {
    return this._instance || (this._instance = new this());
  }

  login(person: ILoginForm): Promise<ILogin> {
    return new Promise((resolve, reject) => {
      this.apiClient.post(this.loginUrl, person)
        .then((res: AxiosResponse) => {
          let login: ILogin = res.data;
          this._currentLogin = login;
          return this.apiClient.get(this.accountUrl + '/' + login.account);
        })
        .then((res: AxiosResponse) => {
          let account: IAccount = res.data;
          this._currentAccount = account;
          resolve(this._currentLogin);
        })
        .catch((err) => reject(err));
    });
  }

}
