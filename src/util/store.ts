export interface IActionType {
  [type: string]: string;
};

export interface IAction {
  type: string;
  value?: any;
}

interface IDispatcher {
  dispatch(action: IAction);
  register(dispatcher: Function);
}

class Dispatcher {
  private listeners: Function[];

  constructor() {
    this.listeners = [];
  }

  dispatch(action: IAction) {
    this.listeners.forEach(listener => listener(action));
  }

  register(listener) {
    this.listeners.push(listener);
  }
}

export abstract class Store {
  protected static storeInstance: Store;
  protected dispatcher: IDispatcher;
  protected listeners: Function[];
  protected state: any;

  protected constructor() {
    this.listeners = [];
    this.dispatcher = new Dispatcher();
    this.state = this.getInitialState();
    this.dispatcher.register(this.onDispatch.bind(this));
  }

  abstract getInitialState(): any;

  abstract onDispatch(action: IAction);

  dispatch(action: IAction) {
    this.dispatcher.dispatch(action);
  }

  addListener(listener: Function) {
    this.listeners.push(listener);
  }

  emitChange() {
    this.listeners.forEach(listener => listener(this.state));
  }
}
