import {ILogger} from './log';

export class ErrorHandler {

    constructor(private logger: ILogger) {
    }

    handlerHttpError(err): string {
      let message = 'An unexpected error occurred';
      if (err.response.status === 404) {
        message = typeof err.response.data === 'object' ? err.response.data.message : err.response.data;
        this.logger.error('404: ' + message);
      } else if (err.response.status === 500) {
        // a nodejs specific error object containing a message and stack
        if (err.response.data.error) {
          message = err.response.data.error.message;
          this.logger.error('500: ' + message + ', stack: ' + err.response.data.error.stack);
        } else {
          message = typeof err.response.data === 'object' ? err.response.data.message : err.response.data;
          this.logger.error('500: ' + message);
        }
      } else {
        this.logger.error(message);
      }
      return message;
    }

}
