import * as Vue from 'vue';
import Component from 'vue-class-component';
import {AxiosError, AxiosResponse, AxiosInstance} from 'axios';
import {ErrorHandler, FlashStore, FlashActionTypes, FlashActions, Logger, SessionService, ILogin, ILoginForm} from '../../util';
import {LogoComponent} from '../logo';
import {MessagesComponent} from '../messages';

@Component({
  template: require('./login.html'),
  components: {
    logo: LogoComponent,
    messages: MessagesComponent
  }
})
export class LoginComponent extends Vue {

  private logger: Logger;
  private errorHandler: ErrorHandler;
  private flashStore: FlashStore;
  private session: SessionService;
  public person: ILoginForm;

  constructor() {
    super();
    this.logger = new Logger();
    this.errorHandler = new ErrorHandler(this.logger);
    this.flashStore = FlashStore.getInstance();
    this.session = SessionService.getInstance();
    this.person = {email: null, password: null};
  }

  signIn() {
    this.flashStore.dispatch(FlashActions.clearMessages());
    let returnPath = this.getQueryParam('returnPath');
    this.session.login(this.person)
      .then((login: ILogin) => {
        localStorage.setItem('sjsac', login.account);
        localStorage.setItem('sjsal', login.autoLogin);
        localStorage.setItem('sjsem', login.email);

        this.$router.push({path: '/admin'});
      })
      .catch((err: AxiosError) => {
        let message = this.errorHandler.handlerHttpError(err);
        this.flashStore.dispatch(FlashActions.addMessage({ level: 'error', text: message }));
      });
  }

  private getQueryParam(name: string): string {
    let map = {};

    if ('' !== window.location.search) {
      let groups = window.location.search.substr(1).split('&'), i;

      for (i in groups) {
        i = groups[i].split('=');
        map[decodeURIComponent(i[0])] = decodeURIComponent(i[1]);
      }
    }

    return map[name];
  }
}
