import * as Vue from 'vue';
import Component from 'vue-class-component';
import {AxiosError, AxiosInstance} from 'axios';
import {Logger, ApiClientFactory, ErrorHandler, FlashStore, FlashActionTypes} from '../../util';
import {LogoComponent} from '../logo';
import {MessagesComponent} from '../messages';

interface ISignup {
  name: string;
  email: string;
  password: string;
}

@Component({
  template: require('./signup.html'),
  components: {
    logo: LogoComponent,
    messages: MessagesComponent
  }
})
export class SignupComponent extends Vue {

  private logger: Logger;
  private errorHandler: ErrorHandler;
  private apiUrl = '/api/account';
  private flashStore: FlashStore;
  private apiClient: AxiosInstance;

  public account: ISignup;
  public formSubmitted: boolean;

  constructor() {
    super();
    this.logger = new Logger();
    this.errorHandler = new ErrorHandler(this.logger);
    this.flashStore = FlashStore.getInstance();
    this.account = {name: null, email: null, password: null};
    this.formSubmitted = false;
    this.apiClient = ApiClientFactory.getClient();
  }

  mounted() {
    this.$nextTick(() => (<HTMLInputElement>document.querySelector('#accountName')).focus());
  }

  signMeUp() {
    this.apiClient.post(this.apiUrl, this.account)
      .then(() => {
        this.flashStore.dispatch({ type: FlashActionTypes.ADD_MESSAGE, value: { level: 'success', text: 'Thanks for signing up! To complete the sign-up, We have emailed you an activation link. Please click the link in the email message to activate your account' } });
        this.formSubmitted = true;
      })
      .catch((err: AxiosError) => {
        let message = this.errorHandler.handlerHttpError(err);
        this.flashStore.dispatch({ type: FlashActionTypes.ADD_MESSAGE, value: { level: 'error', text: message } });
      });
  }
}
