import * as Vue from 'vue';
import Component from 'vue-class-component';
import {Logger} from '../../util';

@Component({
  template: require('./admin.html')
})
export class AdminComponent extends Vue {

  private logger: Logger;

  constructor() {
    super();
    this.logger = new Logger();
  }

  mounted() {
    this.$nextTick(() => this.logger.info('Admin is ready!'));
  }
}
