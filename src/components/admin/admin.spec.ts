import * as Vue from 'vue';
import {ComponentTest} from '../../util/component-test';
import {ILogger} from '../../util/log';

describe('Admin component', () => {
    // ensure component is loaded into webpack
    // modules loaded using es6 imports (as above) that are not used are removed through transpilation
    let adminComponent = require('./admin');

    let directiveTest: ComponentTest;
    let adminComponentInjector: any;
    let infoLoggerSpy = jasmine.createSpy('loggerInfo');

    class MockLogger implements ILogger {
        info(msg: any) {
            infoLoggerSpy(msg);
        }

        warn(msg: any) {
            infoLoggerSpy(msg);
        }

        error(msg: any) {
            infoLoggerSpy(msg);
        }
    }

    beforeEach(() => {
        adminComponentInjector = require('inject-loader!./admin'); // load the module from the webpack bundle

        let mockAdminComponent = adminComponentInjector({
            '../../util/log': { Logger: MockLogger }
        }).AboutComponent;

        directiveTest = new ComponentTest('<div><admin></admin></div>', { 'about': mockAdminComponent });
    });

    it('should render correct contents', (done) => {
        directiveTest.createComponent();
        directiveTest.execute((vm) => {
            expect(vm.$el.querySelector('.repo-link').getAttribute('href')).toBe('https://github.com/ducksoupdev/vue-webpack-typescript');
            expect(infoLoggerSpy).toHaveBeenCalledWith('about is ready!');
            done();
        });
    });
});
