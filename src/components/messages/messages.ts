import * as Vue from 'vue';
import Component from 'vue-class-component';
import {FlashStore, IFlashMessage} from '../../util';

@Component({
    template: require('./messages.html')
})
export class MessagesComponent extends Vue {

  private flashStore: FlashStore;

  messages: IFlashMessage[] = [];

  constructor() {
    super();
    this.flashStore = FlashStore.getInstance();
    this.flashStore.addListener((state) => this.setMessages());
  }

  private setMessages() {
    this.messages = this.flashStore.getMessages();
  }
}
