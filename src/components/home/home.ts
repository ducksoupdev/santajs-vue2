import * as Vue from 'vue';
import Component from 'vue-class-component';
import {Logger, SessionService} from '../../util';
import {LogoComponent} from '../logo';

@Component({
  template: require('./home.html'),
  components: {
    logo: LogoComponent
  }
})
export class HomeComponent extends Vue {

  private logger: Logger;
  private session: SessionService;

  constructor() {
    super();
    this.logger = new Logger();
    this.session = SessionService.getInstance();
  }

  get currentAccount() {
    return this.session.currentAccount;
  }

  get currentLogin() {
    return this.session.currentLogin;
  }

  mounted() {
    this.$nextTick(() => this.logger.info('Home is ready!'));
  }

}
