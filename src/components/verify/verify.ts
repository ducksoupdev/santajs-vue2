import * as Vue from 'vue';
import Component from 'vue-class-component';
import {Logger} from '../../util';

@Component({
    template: require('./verify.html')
})
export class VerifyComponent extends Vue {

    private logger: Logger;

    mounted() {
        if (!this.logger) this.logger = new Logger();
        this.$nextTick(() => this.logger.info('Verify is ready!'));
    }
}
