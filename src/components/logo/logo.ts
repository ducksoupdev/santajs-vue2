import * as Vue from 'vue';
import Component from 'vue-class-component';

@Component({
    template: require('./logo.html')
})
export class LogoComponent extends Vue {

}
