import * as Vue from 'vue';
import Component from 'vue-class-component';
import {AxiosError, AxiosResponse, AxiosInstance} from 'axios';
import {ApiClientFactory, ErrorHandler, FlashStore, FlashActionTypes, Logger} from '../../util';
import {LogoComponent} from '../logo';
import {MessagesComponent} from '../messages';

@Component({
  template: require('./allocate.html'),
  components: {
    logo: LogoComponent,
    messages: MessagesComponent
  }
})
export class AllocateComponent extends Vue {

  private logger: ILogger;
  private errorHandler: IErrorHandler;
  private listByShortUrl = '/api/listByShortUrl';
  private allocateUrl = '/api/allocate';
  private flashStore: IFlashStore;
  private apiClient: AxiosInstance;
  private list: any;

  public formSubmitted: boolean;
  public name: string;

  constructor() {
    super();
    this.formSubmitted = false;
    this.logger = new Logger();
    this.errorHandler = new ErrorHandler(this.logger);
    this.flashStore = FlashStore.getInstance();
    this.apiClient = ApiClientFactory.getClient();
  }

  created() {
    this.apiClient.get(this.listByShortUrl + '/' + this.$route.params['shortUrl'])
      .then((res: AxiosResponse) => {
        this.list = res.data;
        this.allocateUrl = this.allocateUrl + '/' + this.list['_id'];
      })
      .catch((err: AxiosError) => {
        let message = this.errorHandler.handlerHttpError(err);
        this.flashStore.dispatch({ type: FlashActionTypes.ADD_MESSAGE, value: { level: 'error', text: message } });
        this.formSubmitted = true;
      });
  }

  mounted() {
    this.$nextTick(() => (<HTMLInputElement>document.querySelector('#name')).focus());
  }

  allocate() {
    if (this.name != null) {
      this.apiClient.post(this.allocateUrl, {name: this.name})
        .then((res: AxiosResponse) => {
          let personAllocated = res.data;
          this.formSubmitted = true;
          this.flashStore.dispatch({ type: FlashActionTypes.CLEAR_MESSAGES });
          this.flashStore.dispatch({ type: FlashActionTypes.ADD_MESSAGE, value: { level: 'success', text: 'You\'re buying a gift for <strong>' + personAllocated['name'] + '</strong>!' } });
        })
        .catch((err: AxiosError) => {
          let message = this.errorHandler.handlerHttpError(err);
          this.flashStore.dispatch({ type: FlashActionTypes.CLEAR_MESSAGES });
          this.flashStore.dispatch({ type: FlashActionTypes.ADD_MESSAGE, value: { level: 'error', text: message } });
        });
    } else {
      this.flashStore.dispatch({ type: FlashActionTypes.ADD_MESSAGE, value: { level: 'error', text: 'Please enter your name!' } });
    }
  }
}
