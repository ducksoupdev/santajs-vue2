import * as Vue from 'vue';
import * as VueRouter from 'vue-router';

import { SessionService } from './util/session';

// create a session instance
SessionService.createInstance();

import { HomeComponent } from './components/home';
import { AdminComponent } from './components/admin';
import { SignupComponent } from './components/signup';
import { LoginComponent } from './components/login';
import { AllocateComponent } from './components/allocate';
import { VerifyComponent } from './components/verify';

// register the router plugin
Vue.use(VueRouter);

let router = new VueRouter({
  routes: [
    { path: '/', component: HomeComponent },
    { path: '/signUp', component: SignupComponent },
    { path: '/login', component: LoginComponent },
    { path: '/admin', component: AdminComponent },
    { path: '/s/:shortUrl', component: AllocateComponent },
    { path: '/v:/:verificationCode', component: VerifyComponent },
  ]
});

new Vue({
  el: '#app-main',
  router: router
});
